import "dotenv/config";
import express from "express";
import products from "./src/routes/products.js";
import swaggerUiExpress from "swagger-ui-express";
import { swaggerDocs } from "./src/swagger/index.js";

const app = express();
const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Server listening on port ${port}!`));

app.use("/products", products);

app.use("/docs", swaggerUiExpress.serve, swaggerUiExpress.setup(swaggerDocs));

export default app;
