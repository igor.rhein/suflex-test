import { getKnexInstance } from "../database/config.js";

class ProductRepository {
  constructor(knex) {
    this.knex = knex;
  }

  async findAll() {
    return this.knex("products").select();
  }

  async findBy(condition) {
    return this.knex("products")
      .select()
      .where({ ...condition });
  }
}

export const productRepository = new ProductRepository(getKnexInstance());
