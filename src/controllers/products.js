import { productRepository } from "../repository/product.js";

class ProductController {
  async getAll(res) {
    try {
      const products = await productRepository.findAll();
      const sortedProducts = this.sortProducts(products);
      res.send(sortedProducts);
    } catch (error) {
      res.status(400).send({ message: "Erro ao buscar produtos!" });
    }
  }

  async findByCondition(days, res) {
    try {
      const products = await productRepository.findBy({
        days_until_expiration_date: days,
      });
      const sortedProducts = this.sortProducts(products);
      res.send(sortedProducts);
    } catch (error) {
      res.status(400).send({ message: "Erro ao buscar produtos!" });
    }
  }

  sortProducts(products) {
    return products.sort((next, curr) => next.name.localeCompare(curr.name));
  }
}

export const productController = new ProductController();
