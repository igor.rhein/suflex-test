import express from "express";
import { productController } from "../controllers/products.js";
import bodyParser from "body-parser";

const router = express.Router();
router.use(bodyParser.json());

/**
 * @swagger
 * /products:
 *  tags:
 *  - products
 *  get:
 *    description: Fetches all registered products in alphabetic order
 *    responses:
 *      '200':
 *        description: Successfully fetched all products
 *        schema:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              id:
 *                type: integer
 *              name:
 *                type: string
 *              days_until_expiration_date:
 *                type: integer
 *      '400':
 *        description: Failed to fetch products
 */
router.get("/", async ({ res }) => {
  await productController.getAll(res);
});

/**
 * @swagger
 * /products:
 *  post:
 *    description: Fetches all registered products with the same expiration days
 *    produces:
 *      - application/json
 *    consumes:
 *      - application/json
 *    parameters:
 *    - in: body
 *      name: body
 *      schema:
 *        type: object
 *        properties:
 *          days:
 *            type: integer
 *    responses:
 *      '200':
 *        description: Successfully fetched products matching criteria
 *        schema:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              id:
 *                type: integer
 *              name:
 *                type: string
 *              days_until_expiration_date:
 *                type: integer
 *      '400':
 *        description: Failed to fetch products
 */

router.post("/", async (req, res) => {
  const { days } = req.body;
  await productController.findByCondition(days, res);
});

export default router;
