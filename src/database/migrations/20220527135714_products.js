export async function up(knex) {
  await knex.schema.createTable("products", (table) => {
    table.increments("id").primary();
    table.string("name");
    table.integer("days_until_expiration_date");
  });
}

export async function down(knex) {
  await knex.schema.dropTableIfExists("products");
}
