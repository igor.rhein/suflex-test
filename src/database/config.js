import * as Knex from "knex";
import * as config from "../../knexfile.cjs";

export function getKnexInstance() {
  return Knex.default(config);
}
