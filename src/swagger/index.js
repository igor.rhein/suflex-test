import swaggerJsdoc from "swagger-jsdoc";

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "Suflex Products API",
      description: "A simple API created for evaluation",
      contact: {
        name: "Igor Rhein",
      },
      servers: ["http://localhost:8000"],
    },
  },
  apis: ["./src/routes/products.js"],
};

export const swaggerDocs = swaggerJsdoc(swaggerOptions);
