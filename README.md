# SUFLEX TEST API

Repo containing the features previously aligned for evaluation

#

## Get started

After cloning the repo make sure to:

- Create a .env file, you may copy and paste values as it is from file .env.example
- Install dependencies with `npm i` or `npm ci`

#

### 🐋 Quick start (Docker)

```
1 - Run application and database via docker command: `docker-compose up -d`

2 - Run migrations with `npm run migration`

3 - Seed database with `npm run seed`,

4 - Access `http://localhost:8000/docs` for swagger documentation and to consume endpoints
```
